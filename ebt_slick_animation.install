<?php

/**
 * @file
 * Install, update and uninstall functions for the EBT Slick Slider animation module.
 */

use Drupal\media\Entity\MediaType;
use Drupal\field\Entity\FieldConfig;

/**
 * Implements hook_requirements().
 */
function ebt_slick_animation_requirements($phase) {
  if ($phase != 'install') {
    return [];
  }

  if (class_exists('Drupal\media\Entity\MediaType')) {
    foreach (MediaType::loadMultiple() as $type) {
      if ($type->id() == 'image') {
        return [];
      }
    }
  }

  return [
    'ebt_slick_animation_media_type_image' => [
      'title' => t('Media type Image'),
      'value' => t('Not created'),
      'description' => t('The EBT Slick Animation needs to be <a href="@url">created</a> "Image" Media type.', ['@url' => '/admin/structure/media']),
      'severity' => REQUIREMENT_ERROR,
    ],
  ];
}

/**
 * Set the field "Slide Image" as required.
 */
function ebt_slick_animation_update_9101() {

  // Set the machine names.
  $paragraph_type_id = 'ebt_slick_animation';
  $field_name = 'field_ebt_slick_animation_image';

  // Load the field configuration object.
  $field_config = FieldConfig::loadByName('paragraph', $paragraph_type_id, $field_name);

  // If the field doesn't exists, skip.
  if (empty($field_config)) {
    return;
  }

  // Set the field as required.
  $field_config->setRequired(TRUE)->save();

}
